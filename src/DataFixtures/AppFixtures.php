<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\Scores;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{

    private $hasher;
    public function __construct(UserPasswordHasherInterface $hasher){ //fonction construteur pour faire appel a la fonction userpassword...
        $this->hasher=$hasher;
    }
    

    public function load(ObjectManager $manager): void
    {
         // $product = new Product();
        // $manager->persist($product);

        for ($i = 0; $i < 20; $i++) {
            $score = new Scores();
            $score->setScore(mt_rand(10, 100));
            $score->setUser('user'.$i);
            $manager->persist($score);
        }


        $user = new User();
        $user->setEmail('admin@admin.com');
        $user->setRoles(['ROLE_ADMIN']);
        $password = $this->hasher->hashPassword($user, "motdepasse"); // creation variable qui va recup fonction hashpassword appelée dans le constructeur
        $user->setPassword($password);
        $manager->persist($user);


        $manager->flush();
    }
}
