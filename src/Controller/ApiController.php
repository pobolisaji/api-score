<?php

namespace App\Controller;

use App\Entity\Scores;
use App\Repository\ScoresRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/api', name: 'api')]
class ApiController extends AbstractController
{
    
    //=========================//
    //   ENVOI api méthode GET
    // pour récup liste 
    // de tous les scores
    //========================// 
    
    #[Route('/scores', name:'scores')]
    public function scores(ScoresRepository $scoreRepo): Response
    {
        return $this->json($scoreRepo->findAll(), 200, []);
    }

      //=========================//
    //   ENVOI api méthode GET
    //   récupérer selon ID
    //========================// 

    // route pour afficher score selon ID
    #[Route('/scores/{id}', name: 'scores_id', methods: 'GET')]
    public function scoresShow(ScoresRepository $scoreRepo, int $id)
    {
        return $this->json($scoreRepo->findBy(array('id' => $id)), 200, []);
    }


      //=========================//
    //   ENVOI api méthode GET
    //   10 meilleurs resultats
    //========================// 

    #[Route('/scores/liste/best', name:'api_score_best', methods:'GET')] 
    public function ApiScoreBest(ScoresRepository $scoreRepo): Response
    {
        return $this->json($scoreRepo->findByExampleField(), 200, []);
    }


    
      //=========================//
    //   ENVOI api méthode POST
    //   pour ajouter un score
    //========================// 

    // route pour ajouter un score dans la BDD avec méthode POST
    #[Route('/scores/add', name:'scores_add', methods:'POST')]
    public function scoresAdd(Request $request, SerializerInterface $serializer)
    {
        // on decode les données et on désérialise 
        $datas = $request->getContent(); //on recup le json de la requete
        $scores = $serializer->deserialize($datas, Scores::class, 'json');

        // on sauvegarde dans la BDD
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($scores); //prepare 
        $entityManager->flush(); //enregistre dans bdd

        // on renvoie le statut http si c'est ok
        return new Response('OK', 201);
        // on renvoie statut http 404 si erreur
        return new Response('NOT OK', 404);
    }

  
}
